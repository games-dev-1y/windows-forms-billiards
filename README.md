# README #

This is Applied Math Lab 7, where we are to make in Windows Forms a billiards game using fully elastic collisions.

### What is this repository for? ###

* Lab 7 - Billiards
* Version 3.0

### How do I get set up? ###

* Clone repository

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)