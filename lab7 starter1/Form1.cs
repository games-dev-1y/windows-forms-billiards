﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows.Forms;

/*
 * Name: Rafael Girao
 * Student ID: C00203250
 * Project: Lab 7 Part 3:
 *  - Create a Billiards Game
 * 
 * Worked on:
 *  05th/01
 *      from:   11:00
 *      to:     12:00 (1 hour)
 *  05th/01
 *      from:   13:00
 *      to:     18:00 (5 hours)
 *  16th/01
 *      from:   18:00
 *      to:     21:00 (4 hours)
 *  18th/01
 *      from:   08:00
 *      to:     11:00 (3 hours)
 * 
 * Total time spent:
 *      13h:00min
 * 
 * Known bugs:
 *  when balls collide they sometimes stop moving instead of colliding
 */

namespace lab7_starter1
{
    public partial class Form1 : Form
    {
        float width = 1000;
        float height = 700;
        Vector3 radius = new Vector3();
        Vector3 topLeft = new Vector3(5, 5, 0); // blue rectangle
        Vector3 cueBall = new Vector3(); // red ball vector
        Vector3 redBall = new Vector3(); // cue ball vector
        Vector3 yellowBall = new Vector3(); // yellow ball vector
        float diameter = 30;
        float topLeftX = 5;
        float topLeftY = 5;
        Vector3 mouse = new Vector3(); // mouse vector
        Vector3 directionCueBall = new Vector3(); // red ball velocity vector
        Vector3 directionRedBall = new Vector3(); // cue ball velocity vector
        Vector3 directionYellowBall = new Vector3(); // yellow ball velocity vector
        Vector3 pocket01, pocket02, pocket03, pocket04, pocket05, pocket06; // black pockets vectors
        SoundPlayer ballHitSound; // Will play a .wav file when a ball collides with another
        SoundPlayer boundaryHitSound; // Will play a .wav file when a ball hits the edge of the pool table
        SoundPlayer pocketHitSound; // Will play a .wav file when a ball enters a pocket
        float friction; // will decrease the speed over time
        bool drawDirectionLine; // to draw or not
        bool moveBall; // update ball location
        bool turn = true; // whose turn is it, starts as cue ball's turn

        const bool cueBallTurn = true; // to make the turn if statements more human readable
        const bool yellowBallTurn = false; // ^ 
        const float topSpeed = 10; // What speed is reset to.
        const float pocketOffSet = 5; // how much the pocket spawn into the blue outline
        const float bounceFactor = 0.5f; // How much the bouncing off the edge will reduce
        const int pocketDifficulty = 15; // will determine how hard/easy it is to pocket a ball
        const float outOfScreen = -200f; // Used to determine if ball has been removed from screen

        /// <summary>
        /// Setup from and variables
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            width = this.Width - 30;
            height = this.Height - 50;
            radius = new Vector3(diameter / 2, diameter / 2, 0);
            LoadSounds();
            ResetBalls();
            SetPockets();
            friction = 0.99f;
            drawDirectionLine = false;
            moveBall = false;
            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
              ControlStyles.UserPaint |
              ControlStyles.DoubleBuffer, true);

        } // End of Form1()

        /// <summary>
        /// Will loadup the sound files into their sound variables
        /// </summary>
        private void LoadSounds()
        {
            ballHitSound = new SoundPlayer(@"hit.wav");
            boundaryHitSound = new SoundPlayer(@"cushion.wav");
            pocketHitSound = new SoundPlayer(@"drop.wav");

        } // End of LoadSounds()

        /// <summary>
        /// Will play the sound,
        /// stored in the passed variable
        /// </summary>
        /// <param name="sound">passed sound variable</param>
        private void Play(SoundPlayer sound)
        {
            sound.Play();

        } // End of Play()

        /// <summary>
        /// Will set the pockets into their appropriate locations
        /// </summary>
        private void SetPockets()
        {
            pocket01 = new Vector3(topLeft.X - pocketOffSet + radius.X, topLeft.Y - pocketOffSet + radius.Y, 0); // Top left pocket
            pocket02 = new Vector3(topLeft.X + (width / 2), topLeft.Y - pocketOffSet + radius.Y, 0); // top middle pocket
            pocket03 = new Vector3(topLeft.X + width - radius.X + pocketOffSet, topLeft.Y - pocketOffSet + radius.Y, 0); // top right pocket
            pocket04 = new Vector3(topLeft.X - pocketOffSet + radius.X, topLeft.Y + height - radius.Y + pocketOffSet, 0); // bottom left pocket
            pocket05 = new Vector3(topLeft.X + (width / 2), topLeft.Y + height - radius.Y + pocketOffSet, 0); // bottom middle pocket
            pocket06 = new Vector3(topLeft.X + width - radius.X + pocketOffSet, topLeft.Y + height - radius.Y + pocketOffSet, 0); // bottom right pocket

        } // End of SetPockets()

        /// <summary>
        /// Draw the game start with a blue box the size of the form (minus our border)
        /// Draw the line (cue) is setting up a shoot
        /// and then draw the ball
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            // Create pens in blue & black
            Pen bluePen = new Pen(Color.Blue, 2.5f);
            Pen blackPen = new Pen(Color.Black, 4f);
            SolidBrush greenBrush = new SolidBrush(Color.Green);

            // Create a rectangle for the sides of the box
            Rectangle Rect1 = new Rectangle((int)topLeft.X, (int)topLeft.Y, (int)width, (int)height);
            // Fills the box
            e.Graphics.FillRectangle(greenBrush, Rect1);
            // Draw the box
            e.Graphics.DrawRectangle(bluePen, Rect1);
            if (drawDirectionLine == true)
            {
                if (turn == cueBallTurn)
                {
                    e.Graphics.DrawLine(blackPen, mouse.X, mouse.Y, cueBall.X, cueBall.Y);
                }
                else if (turn == yellowBallTurn)
                {
                    e.Graphics.DrawLine(blackPen, mouse.X, mouse.Y, yellowBall.X, yellowBall.Y);
                }
                
            }
            DrawBalls(e, Rect1);

        } // End of Form1_Paint()

        /// <summary>
        /// use a black pen and coloured (red, white, yellow) brush to draw the coloured balls using
        /// an ellipse defined by a square elipse defined by square with the position vector coords at centre
        /// so we compute top left corner
        /// </summary>
        /// <param name="e"></param>
        private void DrawBalls(PaintEventArgs e, Rectangle windowRect)
        {
            Pen blackPen = new Pen(Color.Black, 3);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            SolidBrush whiteBrush = new SolidBrush(Color.White);
            SolidBrush yellowBrush = new SolidBrush(Color.Yellow);
            SolidBrush blackBrush = new SolidBrush(Color.Black);
            Vector3 position = new Vector3();
            RectangleF outerRect = new RectangleF();

            // Draws black pockets
            // pocket 1
            position = new Vector3(pocket01 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);
            // pocket 2
            position = new Vector3(pocket02 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);
            // pocket 3
            position = new Vector3(pocket03 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);
            // pocket 4
            position = new Vector3(pocket04 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);
            // pocket 5
            position = new Vector3(pocket05 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);
            // pocket 6
            position = new Vector3(pocket06 - radius);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(blackBrush, outerRect);

            // Draws cue ball
            position = new Vector3((cueBall.X - radius.X), (cueBall.Y - radius.X), 0);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(whiteBrush, outerRect);

            // Draws red ball
            position = new Vector3((redBall.X - radius.X), (redBall.Y - radius.X), 0);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(redBrush, outerRect);

            // Draws yellow ball
            position = new Vector3((yellowBall.X - radius.X), (yellowBall.Y - radius.X), 0);
            outerRect = new RectangleF(position.X, position.Y, diameter, diameter);
            e.Graphics.DrawEllipse(blackPen, outerRect);
            e.Graphics.FillEllipse(yellowBrush, outerRect);

        } // End of DrawBall()

        /// <summary>
        /// Start of player interaction 
        /// set moveBall and drawDirection Line Booleans
        /// control the drawing mode.
        /// assign mouse vector (used to draw line and later calculate direction)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse = new Vector3(e.X, e.Y, 0);

            if (CheckIfOutsideScreen() == false)
            {
                drawDirectionLine = true;
                moveBall = false;
            }
            
        } // End of Form1_MouseDown()

        /// <summary>
        /// player is dragging mouse so update our mouse co-ords
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            mouse = new Vector3(e.X, e.Y, 0);

        } // End of Form1_MouseMove()

        /// <summary>
        /// When the player releases the mouse workout the
        /// line segment specified by the location of the ball to the last
        /// know sighting of the mouse. Normalise this so it's length is one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            drawDirectionLine = false;
            moveBall = true;

            if (turn == cueBallTurn && CheckIfOutsideScreen() == false)
            {
                directionCueBall = (cueBall - mouse);
                directionCueBall = directionCueBall.Scale(0.1);
                turn = yellowBallTurn;

            }
            else if (turn == yellowBallTurn && CheckIfOutsideScreen() == false)
            {
                directionYellowBall = (yellowBall - mouse);
                directionYellowBall = directionYellowBall.Scale(0.1);
                turn = cueBallTurn;
            }

        } // End of Form1_MouseUp()

        /// <summary>
        /// if the ball is moving (not aiming) then update it's location
        /// and check it's at the edge of the box if so bounce by negating
        /// the vector's x direction on left right boundrys and the vector's y direction on
        /// top and bottom boundries
        /// </summary>
        internal void UpdateWorld()
        {
            if (moveBall == true)
            {
                cueBall += directionCueBall;
                redBall += directionRedBall;
                yellowBall += directionYellowBall;
                ApplyFriction();
            }

            ComputeCollision(ref cueBall, ref directionCueBall, ref redBall, ref directionRedBall);
            ComputeCollision(ref redBall, ref directionRedBall, ref yellowBall, ref directionYellowBall);
            ComputeCollision(ref yellowBall, ref directionYellowBall, ref cueBall, ref directionCueBall);

            CheckIfRestartGame();
            
        } // End of UpdateWorld()

        /// <summary>
        /// Will restart the game if,
        /// any of the balls is outside the screen,
        /// and all balls aren't moving
        /// </summary>
        private void CheckIfRestartGame()
        {
            Vector3 notMoving = new Vector3(0, 0, 0);
            if (directionCueBall == notMoving &&
                directionRedBall == notMoving &&
                directionYellowBall == notMoving)
            {
                if ( CheckIfOutsideScreen() )
                {
                    ResetBalls();
                }
            }

        } // End RestartGame()

        /// <summary>
        /// Will verify any of the balls is outside the screen
        /// </summary>
        /// <returns>returns true if yes, false if not</returns>
        private bool CheckIfOutsideScreen()
        {
            bool outsideScreen;

            if ( (cueBall.X == outOfScreen && cueBall.Y == outOfScreen) ||
                (redBall.X == outOfScreen && redBall.Y == outOfScreen) ||
                (yellowBall.X == outOfScreen && yellowBall.Y == outOfScreen) )
	        {
                outsideScreen = true;
	        }
            else
            {
                outsideScreen = false;
            }

            return outsideScreen;

        } // End of CheckIfOutsideScreen()

        /// <summary>
        /// Will handle all collisions between,
        /// passed ball with all the pockets
        /// </summary>
        /// <param name="ball1">ball vector</param>
        /// <param name="ball1Velocity">ball velocity vector</param>
        private void CheckPocketCollision(ref Vector3 ball1, ref Vector3 ball1Velocity)
        {
            if (CheckRectangleBallCollision(ball1, pocket01) == true)
            {
                if (CheckCircularCollision(ball1, pocket01) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket01);
                }
            }
            else if (CheckRectangleBallCollision(ball1, pocket02) == true)
            {
                if (CheckCircularCollision(ball1, pocket02) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket02);
                }
            }
            else if (CheckRectangleBallCollision(ball1, pocket03) == true)
            {
                if (CheckCircularCollision(ball1, pocket03) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket03);
                }
            }
            else if (CheckRectangleBallCollision(ball1, pocket04) == true)
            {
                if (CheckCircularCollision(ball1, pocket04) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket04);
                }
            }
            else if (CheckRectangleBallCollision(ball1, pocket05) == true)
            {
                if (CheckCircularCollision(ball1, pocket05) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket05);
                }
            }
            else if (CheckRectangleBallCollision(ball1, pocket06) == true)
            {
                if (CheckCircularCollision(ball1, pocket06) == true)
                {
                    PocketCollision(ref ball1, ref ball1Velocity, pocket06);
                }
            }
            
        } // End of PocketCollision

        /// <summary>
        /// Will move the ball out of the screen,
        /// and set its velocity to 0,
        /// if the ball collides with the passed pocket
        /// </summary>
        /// <param name="ball">balls position vector</param>
        /// <param name="ballVelocity">balls velocity vector</param>
        /// <param name="pocket1">pocket to be collided with</param>
        private void PocketCollision(ref Vector3 ball, ref Vector3 ballVelocity, Vector3 pocket1)
        {
            if (ball.X < pocket1.X + pocketDifficulty &&
                ball.X > pocket1.X - pocketDifficulty &&
                ball.Y < pocket1.Y + pocketDifficulty &&
                ball.Y > pocket1.Y - pocketDifficulty)
            {
                RemoveBall(ref ball, ref ballVelocity);
                Play(pocketHitSound);
            }

        } // End of PocketCollision()

        /// <summary>
        /// Will place the ball off the screen,
        /// and set its Z value to 1,
        /// so no boundary collision affects the ball,
        /// until it is reset;
        /// &
        /// sets balls velocity vector to (0,0)
        /// </summary>
        /// <param name="ball">ball position vector to be removed</param>
        private void RemoveBall(ref Vector3 ball, ref Vector3 ballVelocity)
        {
            ball = new Vector3(outOfScreen, outOfScreen, 0);
            ballVelocity = new Vector3(0, 0, 0);

        } // End of RemoveBall()

        /// <summary>
        /// Will run all collision detection methods
        /// and swap vellocity vectors if they collide
        /// </summary>
        private void ComputeCollision(ref Vector3 ball1, ref Vector3 ball1Velocity,
            ref Vector3 ball2, ref Vector3 ball2Velocity)
        {

            if (CheckRectangleBallCollision(ball1, ball2) == true)
            {
                if (CheckCircularCollision(ball1, ball2) == true)
                {
                    //SwapVelocityVectors();
                    ElasticCollision(ref ball1, ref ball1Velocity, ref ball2, ref ball2Velocity);
                }
            }
            
            CheckPocketCollision(ref ball1, ref ball1Velocity);
            if (ball1.X != outOfScreen && ball1.Y != outOfScreen)
            {
                CheckBoundaryCollision(ref ball1, ball1Velocity);
            }

        } // End of ComputeCollision

        /// <summary>
        /// This will run all the necessary operations to perform elastic collision
        /// on the ball vectors and velocity vectors
        /// </summary>
        private void ElasticCollision(ref Vector3 ball1, ref Vector3 ball1Velocty,
            ref Vector3 ball2, ref Vector3 ball2Velocity)
        {
            CalcElasticCollisionVectors(ref ball1, ref ball1Velocty, ref ball2, ref ball2Velocity);
            Play(ballHitSound);

        } // End of ElasticCollision()

        /// <summary>
        /// Subtract the ball centers from each other (collision normal),
        /// turn the collision normal into its unit vector form,
        /// get the parallel components of collision normal on ball 1 velocity,
        /// get the perpendicular components of collision normal on ball 1 velocity,
        /// get the parallel components of collision normal on ball 2 velocity,
        /// get the perpendicular components of collision normal on ball 2 velocity,
        /// to get the resulting velocity's simply,
        /// assign to ball 1 velocity vector the result of,
        /// ball 2 Parallel vector + ball 1 Orthogonal vector;
        /// assign to ball 2 velocity vector the result of,
        /// ball 1 Parallel vector + ball 2 Orthogonal vector.
        /// </summary>
        /// <param name="ball01">ball 1's center vector</param>
        /// <param name="ball01Velocity">ball 1's velocity vector</param>
        /// <param name="ball02">ball 2's center vector</param>
        /// <param name="ball02Velocity">ball 2's velocity vector</param>
        private void CalcElasticCollisionVectors(ref Vector3 ball01, ref Vector3 ball01Velocity, ref Vector3 ball02, ref Vector3 ball02Velocity)
        {
            Vector3 collisionNormal,
                ball01Parallel, ball01Orthogonal,
                ball02Parallel, ball02Orthogonal;

            collisionNormal = ball01 - ball02;
            collisionNormal.Unit();
            
            ball01Parallel = collisionNormal.ParralelComponent(ball01Velocity);
            ball01Orthogonal = collisionNormal.PerpendicularComponent(ball01Velocity);
            ball02Parallel = collisionNormal.ParralelComponent(ball02Velocity);
            ball02Orthogonal = collisionNormal.PerpendicularComponent(ball02Velocity);

            ball01 -= ball01Velocity;
            ball02 -= ball02Velocity;

            ball01Velocity = ball02Parallel + ball01Orthogonal;
            ball02Velocity = ball01Parallel + ball02Orthogonal;

        } // End of CalcElastic CollisionVectors

        /// <summary>
        /// Verifies if the ball has collided with the boundary box
        /// </summary>
        /// <param name="ball">The Vector3 of the ball to be checked</param>
        /// <param name="direction">The Vector3 of the ball direction to be altered</param>
        private void CheckBoundaryCollision(ref Vector3 ball, Vector3 direction)
        {
            // Checks if the ball hit the left side of the boundary box
            if (ball.X < topLeftX + radius.X)
            {
                ball = new Vector3(topLeftX + radius.X, ball.Y, ball.Z);
                direction.BounceX(bounceFactor);
                Play(boundaryHitSound);
            }
            // Checks if the ball hit the right side of the boundary box
            if (ball.X > topLeftX + width - radius.X)
            {
                ball = new Vector3((topLeftX + width - radius.X), ball.Y, ball.Z);
                direction.BounceX(bounceFactor);
                Play(boundaryHitSound);
            }
            // Checks if the ball hit the top side of the boundary box
            if (ball.Y < topLeftY + radius.X)
            {
                ball = new Vector3(ball.X, (topLeftY + radius.X), ball.Z);
                direction.BounceY(bounceFactor);
                Play(boundaryHitSound);
            }
            // Checks if the ball hit the bottom side of the boundary box
            if (ball.Y > topLeftY + height - radius.X)
            {
                ball = new Vector3(ball.X, (topLeftY + height - radius.X), ball.Z);
                direction.BounceY(bounceFactor);
                Play(boundaryHitSound);
            }

        } // End of CheckBoundaryCollision

        /// <summary>
        /// Creates a temporary velocity vector
        /// it will store the red ball velocity vector
        /// than red ball velocity will swap with
        /// the cue ball velocity vector
        /// and vice - versa
        /// </summary>
        private void SwapVelocityVectors()
        {

            cueBall -= directionCueBall;
            redBall -= directionRedBall;
            
            Vector3 tempVelocityVector;

            tempVelocityVector = directionRedBall;
            directionRedBall = directionCueBall;
            directionCueBall = tempVelocityVector;

        } // End of SwapVelocityVectors()

        /// <summary>
        /// Simply applies Friction to the velocity vectors,
        /// by multiplying friction into each velocity vector,
        /// also when the velocity vector squared reaches a low enough value
        /// set it to 0 (full stop)
        /// </summary>
        private void ApplyFriction()
        {
            // lowest Units per pixel, the balls cannot move slower than this, if yes than set the velocity vectors to 0
            const int lowestSpeed = 3;
            directionCueBall = directionCueBall.Scale(friction);
            directionRedBall = directionRedBall.Scale(friction);
            directionYellowBall = directionYellowBall.Scale(friction);

            // Checks if the cue ball velocity vector squared is low enough to set to 0
            if (directionCueBall.X * directionCueBall.X < lowestSpeed && directionCueBall.Y * directionCueBall.Y < lowestSpeed)
            {
                directionCueBall = new Vector3(0, 0, 0);
            }
            // Checks if the red ball velocity vector squared is low enough to set to 0
            if (directionRedBall.X * directionRedBall.X < lowestSpeed && directionRedBall.Y * directionRedBall.Y < lowestSpeed)
            {
                directionRedBall = new Vector3(0, 0, 0);
            }
            // Checks if the yellow ball velocity vector squared is low enough to set to 0
            if (directionYellowBall.X * directionYellowBall.X < lowestSpeed && directionYellowBall.Y * directionYellowBall.Y < lowestSpeed)
            {
                directionYellowBall = new Vector3(0, 0, 0);
            }

        } // End of ApplyFriction()

        /// <summary>
        /// Attributes a rectangle between 2 ball vectors,
        /// checks if the rectangles are intersecting,
        /// than returns true if they collided,
        /// </summary>
        /// <param name="firstBall">The first ball as a Vector3 class</param>
        /// <param name="secondBall">The second ball as a Vector3 class</param>
        /// <returns>Returns true if they collided, false if not</returns>
        private bool CheckRectangleBallCollision(Vector3 firstBall, Vector3 secondBall)
        {
            bool collided;
            Vector3 BallTopLeft = (firstBall - radius);
            Rectangle firstBallCollisionRectangle =
                new Rectangle((int)BallTopLeft.X, (int)BallTopLeft.Y, (int)diameter, (int)diameter);

            BallTopLeft = (secondBall - radius);
            Rectangle secondBallCollisionRectangle =
                new Rectangle((int)BallTopLeft.X, (int)BallTopLeft.Y, (int)diameter, (int)diameter);

            //if (firstBallCollisionRectangle.IntersectsWith(secondBallCollisionRectangle))
            //{
            //    collided = true;
            //}
            //else
            //{
            //    collided = false;
            //}

            // Checks if the ball did NOT collide with the left side of the 2nd ball
            if (firstBallCollisionRectangle.X + firstBallCollisionRectangle.Width < secondBallCollisionRectangle.X)
	        {
                collided = false;
	        }
            // Checks if the ball did NOT collide with the right side of the 2nd ball
            else if (firstBallCollisionRectangle.X > secondBallCollisionRectangle.X + secondBallCollisionRectangle.Width)
            {
                collided = false;
            }
            // Checks if the ball did NOT collide with the top side of the 2nd ball
            else if (firstBallCollisionRectangle.Y + firstBallCollisionRectangle.Height < secondBallCollisionRectangle.Y)
            {
                collided = false;
            }
            // Checks if the ball did NOT collide with the bottom side of the 2nd ball
            else if (firstBallCollisionRectangle.Y > secondBallCollisionRectangle.Y + secondBallCollisionRectangle.Height)
            {
                collided = false;
            }
            // if all previous statements are false than the 1st ball has collided with the 2nd ball
            else
            {
                collided = true;
            }
            

            return collided;

        } // End of CheckBallCollision()

        /// <summary>
        /// Checks for collision between 2 vectors based as parameters,
        /// by getting the distance between the first and second circle's x,y components,
        /// getting the radii from the 2 circle radius'
        /// comparing using the following algorithm
        /// (distanceOfX * distanceOfX) + (distanceOfY * distanceY) greater than (diameter * diameter)
        /// </summary>
        /// <param name="firstCircle">the first circle compared as a Vector3 class</param>
        /// <param name="secondCircle">the second circle compared as a Vector3 class</param>
        /// <returns>Returns true if circles collided,
        /// returns false if circles did not collide</returns>
        private bool CheckCircularCollision(Vector3 firstCircle, Vector3 secondCircle)
        {
            bool collided;
            Vector3 distance = secondCircle - firstCircle;
            // int radii = (int)(diameter);  radius.X + radius.X

            if (  ( (distance.X * distance.X) + (distance.Y * distance.Y) ) < ( diameter * diameter )  )
            {
                collided = true;
            }
            else
	        {
                collided = false;
	        }

            return collided;

        } // End of CheckCircularCollision()
        
        /// <summary>
        /// Sets the balls back to the center of the screen
        /// </summary>
        private void ResetBalls()
        {
            Vector3 center = new Vector3(topLeft.X + (width/2), topLeft.Y + (height/2), 0);

            redBall = new Vector3(center);
            cueBall = new Vector3(center.X - 100, center.Y, 0);
            yellowBall = new Vector3(center.X + 100, center.Y, 0);

        } // End of ResetBalls()

        /// <summary>
        /// Adjust our bounding box to match the from size, take onto account the different borders
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Resize(object sender, EventArgs e)
        {
            width = this.Width - 30;
            height = this.Height - 50;
            ResetBalls();
            SetPockets();

        } // End of Form1_Resize()

    } // End of class

} // End of namespace
