﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/*
 * Name: Rafael Girao
 * Student Number: C00203250
 * From Project 06: To complete a unfinished vector calculator
 * 
 * Worked on:
 *  13th/11
 *      from:   13:45
 *      to:     14:15 (00 hours 30 minutes)
 *  13th/11
 *      from:   15:00
 *      to:     15:30 (00 hours 30 minutes)
 *  13th/11
 *      from:   18:00
 *      to:     18:30 (00 hours 30 minutes)
 *  26th/11
 *      from:   09:15
 *      to:     09:45 (00 hours 30 minutes)
 * 
 * Total time spent:
 *      02h:00min
 * 
 * Known bugs:
 * None
 */

namespace lab7_starter1
{
    class Vector3
    {
        private float x;
        private float y;
        private float z;
        public float Z
        {
            get { return z; }
            set { z = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }
        /// <summary>
        /// default constructor, makes null vector
        /// </summary>
        public Vector3()
        {
            x = 0.0f;
            y = 0.0f;
            z = 0.0f;
        }
        /// <summary>
        /// Constructor taking values for x, y and z
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        public Vector3(float x1, float y1, float z1)
        {
            x = x1;
            y = y1;
            z = z1;
        }

        public Vector3(string textX, string textY, string textZ)
        {
            if (!float.TryParse(textX, out x))
            {
                MessageBox.Show("Bad Number X", "Input Error", MessageBoxButtons.OK);
                x = 0;
                y = 0;
                z = 0;
                return;
            }
            if (!float.TryParse(textY, out y))
            {
                MessageBox.Show("Bad Number Y", "Input Error", MessageBoxButtons.OK);
                x = 0;
                y = 0;
                z = 0;
                return;
            }
            if (!float.TryParse(textZ, out z))
            {
                MessageBox.Show("Bad Number Z", "Input Error", MessageBoxButtons.OK);
                x = 0;
                y = 0;
                z = 0;
                return;
            }

        }
        /// <summary>
        /// constructor taking a vector as the source
        /// </summary>
        /// <param name="v"></param>
        public Vector3(Vector3 v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
        }
        /// <summary>
        /// override for ToString method outputting brackets, comas and formatting the
        /// values as general 3 digits
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "(" + x.ToString("g3") + "," + y.ToString("g3") + "," + z.ToString("g3") + ")";
        }
        /// <summary>
        /// Using the distance formula,
        /// of square.root(x^2 + y^2 + z^2)
        /// returns the length of a vector
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            return Math.Sqrt(  Math.Pow((double)x, 2) + Math.Pow((double)y, 2) + Math.Pow((double)z, 2)  );
        }

        public bool Equals(Vector3 v2)
        {
            if (x == v2.x && y == v2.y && z == v2.z)
                return true;
            else return false;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Vector3 return false.
            Vector3 v2 = obj as Vector3;
            if ((System.Object)v2 == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == v2.x && y == v2.y && z == v2.z);
        }

        public override int GetHashCode()
        {
            return (int)(x * y * z);
        }

        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(v1, v2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)v1 == null) || ((object)v2 == null))
            {
                return false;
            }
            return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
        }

        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            return !(v1 == v2);
        }
        /// <summary>
        /// Will return the dot product of 2 vectors
        /// </summary>
        /// <param name="v2"></param>
        /// <returns></returns>
        public double DotProduct(Vector3 v2)
        {
            return ((x * v2.x) + (y * v2.y) + (z * v2.z));
        }
        /// <summary>
        /// Using the cross product formula
        /// Returns the cross product between 2 vectors
        /// </summary>
        /// <param name="v2"></param>
        /// <returns></returns>
        public Vector3 CrossProduct(Vector3 v2)
        {
            return new Vector3(y * v2.z - z * v2.y, z * v2.x - x * v2.z, x * v2.y - y * v2.x);
        }
        /// <summary>
        /// Using the cosine formula
        /// Returns the angle between 2 vectors
        /// The extra variables are made so that its more readable
        /// </summary>
        /// <param name="v2"></param>
        /// <returns></returns>
        public double AngleBetween(Vector3 v2)
        {
            double dotProduct = (x * v2.x) + (y * v2.y) + (z * v2.z);
            double lengthOfV1 = Math.Sqrt(  Math.Pow((double)x, 2) + Math.Pow((double)y, 2) + Math.Pow((double)z, 2)  );
            double lengthOfV2 = Math.Sqrt(  Math.Pow((double)v2.x, 2) + Math.Pow((double)v2.y, 2) + Math.Pow((double)v2.z, 2));
            double angleInRadians = Math.Acos( (dotProduct) / (lengthOfV1*lengthOfV2) );
            double angleInDegrees = angleInRadians * (180 / Math.PI);
            return angleInDegrees;
        }
        /// <summary>
        /// Will alter the vector coordinates
        /// to its corresponding unit vector
        /// by getting its dividing each coordinate by the vector's length
        /// </summary>
        /// <returns></returns>
        public Vector3 Unit()
        {
            return new Vector3(Scale(1/Length()));
        }


        /// <summary>
        /// Gets the Parralel Components (also called projection) between
        /// the current vector and the 2nd vector (passed as argument)
        /// using the following formula:
        /// (v1.v1 / v1.v2) * v1
        /// v1 = current vector
        /// v2 = 2nd vector
        /// </summary>
        /// <param name="v2">vector to project onto</param>
        /// <returns></returns>
        public Vector3 ParralelComponent(Vector3 v2)
        {
            double v1DotV1Product, v1DotV2Product, scalar;

            v1DotV1Product = DotProduct(this);
            v1DotV2Product = DotProduct(v2);
            scalar = v1DotV2Product / v1DotV1Product;

            return new Vector3(this.Scale(scalar));
        }
        /// <summary>
        /// Gets the Perpendicular components (also called normal) between
        /// the current vector and the 2nd vector (passed as argument)
        /// using the following formula:
        /// v2 - [(v1.v1 / v1.v2) * v1]
        /// v1 = current vector
        /// v2 = 2nd vector
        /// since, (v1.v1 / v1.v2) * v1 = ParralelComponent
        /// the ParralelComponent(v2) is used.
        /// </summary>
        /// <param name="v2"></param>
        /// <returns></returns>
        public Vector3 PerpendicularComponent(Vector3 v2)
        {
            return new Vector3(v2 - ParralelComponent(v2));
        }
        /// <summary>
        /// Convert the scale from a double to a float
        /// </summary>
        /// <param name="scale"></param>
        /// <returns></returns>
        public Vector3 Scale(double scale)
        {
            return Scale((float)scale);
        }
        /// <summary>
        /// Applies a scale to a vector
        /// essentially multiplying the scale into
        /// each of the vector's coordinates
        /// </summary>
        /// <param name="scale"></param>
        /// <returns></returns>
        public Vector3 Scale(float scale)
        {
            return new Vector3(x * scale, y * scale, z * scale);
        }
        /// <summary>
        /// Will add first vector's coordinates (x,y,z)
        /// to the corresponding second vector's coordinates (x,y,z)
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }
        /// <summary>
        /// Subtract second vector from first
        /// return new vector passing difference between components
        /// as values for constructor
        /// </summary>
        /// <param name="v1">first operand</param>
        /// <param name="v2">second operand</param>
        /// <returns></returns>
        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }
        /// <summary>
        /// An overloaded operator - to return the negation of a single vector
        /// return a new vector negating each of the components
        /// </summary>
        /// <param name="v1">vector</param>
        /// <returns></returns>
        public static Vector3 operator -(Vector3 v1)
        {
            return new Vector3(-v1.x, -v1.y, -v1.z);
        }
        /// <summary>
        /// Negates the x coordinate
        /// </summary>
        public void FlipX()
        {
            x = -x;

        } // End of FlipX()
        /// <summary>
        /// Negates the y coordinate
        /// </summary>
        public void FlipY()
        {
            y = -y;

        } // End of FlipY()

        /// <summary>
        /// Will flip x coordinate and 
        /// than reduce by a factor
        /// </summary>
        /// <param name="factor">enter values between 1.0 and 0.0</param>
        public void BounceX(float factor)
        {
            FlipX();
            x *= factor;

        } // End of BounceX()

        /// <summary>
        /// Will flip y coordinate and 
        /// than reduce by a factor
        /// </summary>
        /// <param name= "factor">enter values between 1.0 and 0.0</param>
        public void BounceY(float factor)
        {
            FlipY();
            y *= factor;

        } // End of BounceY()

        /// <summary>
        /// Negates the x and y coordinates
        /// </summary>
        public void FlipXY()
        {
            x = -x;
            y = -y;

        } // End of FlipXY()

    } // End of Vector3 class

} // End of namespace